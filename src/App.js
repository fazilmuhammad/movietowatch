import React, { useEffect, useState } from 'react';
import NavBar from './Components/navbar/NavBar';
import Banner  from './Components/banner/Banner';
import SearchBar from './Components/searchbar/SearchBar';
import Grid from './Components/grid/Grid';
import Modals from './Components/modals/Modals';
import Footer from './Components/footer/Footer';
import Pagination from './Components/pagination/Pagination';
import './App.css';

const App = () => {

    const [data, setData] = useState([]);
    const [dataDetail, setDataDetail] = useState([])
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [q, setQuery] = useState('harry');
    const [activateModal, setActivateModal] = useState(false);
    const [detail, setShowDetail] = useState(false);
    const [detailRequest, setDetailRequest] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(8);
    const API_KEY = '7819d7f3';


    useEffect(() => {

        setLoading(true);
        setError(null);
        setData(null);

        fetch(`http://www.omdbapi.com/?s=${q}&apikey=${API_KEY}`)
        .then(resp => resp)
        .then(resp => resp.json())
        .then(response => {
            if (response.Response === 'False') {
                setError(response.Error);
            }
            else {
                setData(response.Search);
            }

            setLoading(false);
        })
        .catch(({message}) => {
            setError(message);
            setLoading(false);
        })

    }, [q]);


    console.log(dataDetail)


    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = data ? data.slice(indexOfFirstPost, indexOfLastPost) : data;
    const paginate = pageNumber => setCurrentPage(pageNumber);
 

    
    return (
            <>
                <NavBar />
                <div className='container'>
                    <Banner />
                    <SearchBar searchHandler={setQuery} />
                    <Grid
                    setDataDetail={setDataDetail}   
                    ShowDetail={setShowDetail} 
                    DetailRequest={setDetailRequest} 
                    ActivateModal={setActivateModal} 
                    currentPosts={currentPosts} 
                    loading ={loading} 
                    error = {error} 
                    API_KEY={API_KEY}
                    detail = {detail} 
                    {...detail} 
                    /> 
                    <Pagination
                    currentPage={currentPage}
                    postsPerPage={postsPerPage}
                    totalPosts={data ? data.length : 8}
                    paginate={paginate}
                    />
                    <Modals 
                    activateModal={activateModal} 
                    setActivateModal={setActivateModal} 
                    detailRequest= {detailRequest} 
                    dataDetail = {dataDetail} 
                    {...dataDetail}
                    
                    />
                        
                      
                </div>
                <Footer/>
            </>
    );
    
}

export default App;
