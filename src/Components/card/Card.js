import React, { useEffect, useState } from 'react'



const Card = ({setDataDetail, Title,Poster,Year,Type,imdbID,ShowDetail, DetailRequest, ActivateModal, API_KEY}) => {
 
   
    const clickHandler = ({imdbID}) => {
      fetch(`http://www.omdbapi.com/?i=${imdbID}&apikey=${API_KEY}`)
      .then(response => {
        return response.json()
      })
      .then(data => {
        setDataDetail(data)
      })
    }


  console.log(imdbID)

    return (

        <div className='card'>
        <div className='card-inner'>
          <div className='card-front'  >
          <img
                alt={Title}
                src={Poster === 'N/A' ? 'https://placehold.it/198x264&text=Image+Not+Found' : Poster}
                />
          </div>
          {/* onClick={() => clickHandler()} */}
          <div className='card-back' >
            <h1>{Title}</h1>
            <ul>
              <li>
              <div className='additional'>

                <div class="badge">
                  <span class="badge__icon">
                 <i class='bx bxs-time' ></i>
                </span>
                <span class="badge__text">{Year}</span>
                </div>

                <div class="badge">
                  <span class="badge__icon">
                <i class='bx bxs-purchase-tag-alt' ></i>
                </span>
                <span class="badge__text">{Type}</span>
                </div>
              </div>
              </li>
              <li>
              <div className='btnMore'>
              <a onClick={() => clickHandler({imdbID})} href='#demo-modal' class="ntn ntnhov">More</a>
              </div>
              </li>
             
            </ul>
          </div>
        </div>
      </div>




        
    )
}

export default Card