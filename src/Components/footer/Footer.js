import React from 'react'

const Footer = () => {
  return(
    <footer class="footer-distributed">

    <div class="footer-right">

        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>

    </div>

    <div class="footer-left">

        <p class="footer-links">
            <a class="link-1" href="#">Movietowatch</a>
        </p>

        <p>By Bookingtogo.com &copy; 2022</p>
    </div>

</footer>
  )
}

export default Footer