import React, { useState } from "react";
import Image from '../../assets/logo.png'


function NavBar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  return (
    <>
      <nav className="navbar">
        <div className="nav-container">
         <img src={Image} className="logo"/>
         <h1 className="nav-logo">Movietowatch.com</h1>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item nav-links ">
                <a href="#"> Home</a>
            </li>
            <li className="nav-item nav-links">
                <a href="#"> Favorit</a>
            </li>
            <li className="nav-item nav-links">
                <a href="#"> Login</a>
            </li>
          </ul>
          <div className="nav-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"}></i>
          </div>
        </div>
      </nav>
    </>
  );
}

export default NavBar;