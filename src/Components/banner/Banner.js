import React from 'react'

const Banner = () => {
    return (
        <header  className='center banners'>
           <h1 className='banner'>Pengalaman Menonton<br/>Tanpa Batas</h1>
           <p className='subanner'>Nonton dimana pun, cari film sesukanya</p>
        </header>
      )
}

export default Banner