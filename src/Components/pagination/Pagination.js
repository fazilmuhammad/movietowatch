import React from 'react';

const Pagination = ({ currentPage, postsPerPage, totalPosts, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav className='search' >
    <div className="pagination center">
          {pageNumbers.map(number => (
              <a onClick={() => paginate(number)} className={currentPage == number ? 'aactive' : ''} href='!#'>
                {number}
              </a>
          ))}
    </div>
    </nav>
  );

};

export default Pagination;
