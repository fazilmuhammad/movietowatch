import React from 'react'
import Loader from '../loader/Loader'
import MovDetail from '../movdetail/MovDetail'
import { Modal } from 'antd'
// import '../../../node_modules/antd/dist/antd.css'

const Modals = ({activateModal, setActivateModal, detailRequest, dataDetail }) => {
 

var Genre = dataDetail.Genre ? dataDetail.Genre.split(',') : ['1', '2', '3'];
console.log(Genre);

  return (
    <>
    {/* <Modal
        id="demo-modal"
        title='Detail'
        centered
        visible={activateModal}
        onCancel={() => setActivateModal(false)}
        footer={null}
        width={800}
        >
        { detailRequest === false ?
         (<MovDetail {...detail} />) :
         (<Loader />) 
          }
      </Modal> */}

      <div id="demo-modal" class="modal">
     
         <div class="modal__content">
         <div class="movie__hero">
            <img src={dataDetail.Poster} alt="Rambo" class="movie__img"/>
          </div>
          <div className='headermodal'>
          <h1>{dataDetail.Title}</h1>
          <div className='genreBadge'>
          {
                
                 Genre.map(item => {
                  return ( <div class="badge">
                <span class="badge__text">{item}</span>
                </div>
                  )
                })}
          </div>
      
   

          <div class="movdt">
          <span class="movdt__icon">
          <i class='bx bx-time' ></i>
          </span>
           <span class="movdt__text">{dataDetail.Runtime} </span>
          </div>

          <div class="movdt">
          <span class="movdt__icon">
          <i class='bx bx-calendar' ></i>
          </span>
           <span class="movdt__text">{dataDetail.Year} </span>
          </div>

          <div class="movdt">
          <span class="movdt__icon">
          <i class='bx bx-flag' ></i>
          </span>
           <span class="movdt__text">{dataDetail.Country} </span>
          </div>

          <div class="movdt">
          <span class="movdt__icon">
          <i class='bx bx-globe' ></i>
          </span>
           <span class="movdt__text">{dataDetail.Language} </span>
          </div>

          
          <div className='mov_prod'>
          <p>Director: </p>
          <span class="mov_prod_text">{dataDetail.Director} </span>
          </div>
          <div className='mov_prod'>
          <p>Actor: </p>
          <span class="mov_prod_text">{dataDetail.Actors} </span>
          </div>
          
          <p class="movie__description">
          {dataDetail.Plot}
          </p>


          </div>
          

       
        
         </div>
      <div className='btnmodal'>
      <a href="#" class="modal__close">Close</a>
      <a href="#" class="modal__watch">Watch</a>
      </div>
     </div>


    
    </>
     


  )
}

export default Modals