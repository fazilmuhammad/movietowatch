import React from 'react'
import Card from '../card/Card'
import Loader from '../loader/Loader'
import { Alert } from 'antd';

const Grid = ({setDataDetail, setShowDetail, setDetailRequest, setActivateModal, currentPosts, loading, error, API_KEY,detail}) => {
  return (
      <>
        { loading &&
            <Loader className='center' />
        }

        { error !== null &&
            <>
                {/* <Alert message={error} type="error"/> */}
               <div class="Message Message--red">
                <div class="Message-body">
                    <h2>Ups filmnya ngak ketemu :(</h2>
                    <p>Pastikan tidak kosong, dan <br/>nama atau kategori benar</p>
                    <button onClick= {window.location.reload()} class="Message-button" id="js-helpMe">Cari Lagi</button>
                </div>
                </div>
                
            </>
        }
     <section className='cards'>
      
                            
                            
            { currentPosts !== null && currentPosts.length > 0 && currentPosts.map((result, index) => (
                                
            <Card
                setDataDetail={setDataDetail}
                ShowDetail={setShowDetail} 
                DetailRequest={setDetailRequest}
                ActivateModal={setActivateModal}
                API_KEY = {API_KEY}
                key={index} 
                {...result} 
                detail = {detail} 
                {...detail}
            />

            ))}
     </section>
     </>
  )
}

export default Grid